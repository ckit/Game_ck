# name:ck
# Time:2019/10/22 上午11:40
# File:taobao.py
# coding:utf8

import os
import time


def Tao(i):
    os.system('adb shell input tap {}'.format(i))
    print('执行点击屏幕，等待两秒')
    time.sleep(2)

    for _ in range(5):
        print('执行滑动屏幕,循环三次')
        # 翻页  adb shell input swipe 开始位置<513 1980> 结束位置< 513 359 > 翻页时间/ms < 500 >
        os.system('adb shell input swipe 513 1580 513 359 500 ')

    print('页面等待15s')
    time.sleep(12)

    os.system('adb shell input tap 875 2099')
    print('执行返回操作')


if __name__ == '__main__':
    for _ in range(3):
        Tao(input("输入坐标:"))
