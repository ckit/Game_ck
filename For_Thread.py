import os
import time
from threading import Thread


# adb shell input tap tap shell中的点击命令,+ x,y轴,的坐标
def home():
    os.system('adb shell input tap 679 962')


def peoples():
    os.system('adb shell input tap 537 2040')


def maney():
    os.system('adb shell input tap 791 1375')


# 定义计数器
time_ = 0
# 设置线程的嵌套循环次数
for i in range(10000000000000):
    # print('执行时间：', localtime)
    # 设置线程每次循环的休息时间
    time.sleep(0.21)
    time_ += 1
    # print('num__',time_)
    # 这里是将每一次循环作为一次新的线程，一个线程执行一次th=Thread（）函数。
    for _ in range(1):
        localtime = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
        print('th')
        #  增加一个线程
        th = Thread(target=home)
        # th = Thread(target=peoples)
        # th = Thread(target=maney)
        # th = Thread(target=maney_)
        # 开启线程
        th.start()
        # th.join()   # join作用： 等这个线程运行完毕，程序再往下运行 829  2081
        print('执行次数：', time_, '\n', '执行时间：', localtime)

"""
线程 循环
"""
