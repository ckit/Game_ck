"""   抖音点赞   """
import os
import time
from PIL import Image


def screen():
    # 截图保存在手机上
    os.system('adb shell screencap -p /sdcard/b.png')
    # 图片传入电脑上
    os.system('adb pull /sdcard/b.png')

# 调用截图函数
# screen()

def getDistance():
    # 读取图片
    image = Image.open('b.png')

    # 返回的是元组，打印第0个和第1个值
    # 获取图片宽度1080
    width = image.size[0]
    # 获取图片高度2160
    height = image.size[1]

    print('----宽和高----', width, height)
    for w in range(909, 910):
        for h in range(0, height):
            data = image.getpixel((w, h))[:3]
            if data == (255, 155, 174):
                yield w, h


# getDistance()
if __name__ == '__main__':

    # 执行次数
    for _ in range(2):

        screen()
        xPosition = getDistance()

        for x in xPosition:
            # print(x)
            # 点击屏幕
            os.system('adb shell input tap 909 {}'.format(x[1]))

            print('正在关注.....')

            # 翻页  adb shell input swipe 开始位置<513 1980> 结束位置< 513 359 > 翻页时间/ms < 500 >
        os.system('adb shell input swipe 513 1580 513 359 500 ')
        time.sleep(0.1) # 休息
        print('正在翻页.....')


"""   抖音点赞   """
