import multiprocessing
import os
from PIL import Image
import threading



def screen():
    # 截图保存在手机上
    os.system('adb shell screencap -p /sdcard/a.png')
    # 图片传入电脑上
    os.system('adb pull /sdcard/a.png')


# 调用截图函数
# screen()

def getDistance():
    # 读取图片
    image = Image.open('a.png')

    # 返回的是元组，打印第0个值
    # 获取图片高度2160
    height = image.size[0]
    # 获取图片宽度1080
    width = image.size[1]

    # print(height,width)
    for w in range(710, 715):
        for h in range(1000, 1010):
            data = image.getpixel((w, h))[:3]
            if data == (103, 164, 255):
                return w, h


def sums():
    xPopition = getDistance()  # 951 892
    y = xPopition[1]
    os.system('adb shell input tap 712 {}'.format(y))
    #     # 正在点击   


if __name__ == '__main__':
    # 创建进程池
    pool = multiprocessing.Pool(2)  # 每次跑Pool(**)个 累计跑range(**)个
    # 执行range（）次          每次20个
    for _ in range(1000000):
        # 开启异步进程
        sun_run = pool.apply_async(sums)
    # 如果进程池 确定个数，运行时不让添加进程  ，进程保护的一种
    pool.close()
    # 异步进程  阻塞进程  所有的子进程挂了 主进程才能挂
    pool.join()


    """
    线程池：
    Home 连击  $$$$
    
    """